#include "solver.h"

void CalculateReconstructionParameters(Input_Data *InputData, Mesh_Data *MeshData)
{
 // Calculation of reconstruction parameters 
 
 Nodes *node = (*MeshData).node;
 Elements *element = (*MeshData).element;
 register int Number_of_Elements = (*MeshData).Number_of_Elements; 
 
 (*MeshData).recon = malloc(sizeof(Reconstruction_Variables) * Number_of_Elements * (*InputData).viscous);
 Reconstruction_Variables *recon = (*MeshData).recon; 
 if (((*InputData).viscous != 0) && (recon == NULL))
 {
  printf("Could not declare reconstruction parameters. Out of memory.\n");
  exit(1);
 }

 if ((*InputData).viscous != 0)
 {
  register int en1, en2, en3, ecx, ecy;
  register int r;
  for (r = 0; r < Number_of_Elements; r++)
  {   
   en1 = element[r].node1 - 1;
   en2 = element[r].node2 - 1;
   en3 = element[r].node3 - 1;
   ecx = element[r].cx;
   ecy = element[r].cy;
           
   recon[r].dx1 = (node[en1].x + node[en2].x)/2 - ecx;
   recon[r].dy1 = (node[en1].y + node[en2].y)/2 - ecy;
   recon[r].dx2 = (node[en2].x + node[en3].x)/2 - ecx;
   recon[r].dy2 = (node[en2].y + node[en3].y)/2 - ecy;
   recon[r].dx3 = (node[en3].x + node[en1].x)/2 - ecx;
   recon[r].dy3 = (node[en3].y + node[en1].y)/2 - ecy;    
  }
 }
}
