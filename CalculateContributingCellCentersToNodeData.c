#include "solver.h"

void CalculateContributingCellCentersToNodeData(Input_Data *InputData, Mesh_Data *MeshData)
{
 // Calculation of cell-center data contributing to nodes 

 Nodes *node = (*MeshData).node;
 Elements *element = (*MeshData).element;
 register int Number_of_original_Nodes = (*MeshData).Number_of_original_Nodes; 
 register int Number_of_Elements = (*MeshData).Number_of_Elements; 
 register int Number_of_line_elements = (*MeshData).Number_of_line_elements; 

 (*MeshData).nd = malloc(sizeof(Node_data) * Number_of_original_Nodes);
 Node_data *nd = (*MeshData).nd; 
 if (nd == NULL)
 {
  printf("Could not allocate memory for CalculateContributingCellCentersToNodeData. \n");
  exit(1);
 }
 else
 {
  if (((*InputData).readfile == 0) && ((*InputData).readdata == 0))
  { 
   FILE *fpnw;
   fpnw = fopen("node.txt", "w");
   if (fpnw == NULL)
   {
    printf("File node.txt could not be generated.\n");
   }
  
   // Finding the number of contributing cell-centers
  
   register int p1;
   for (p1 = 0; p1 < Number_of_original_Nodes; p1++)
   {
    register int flg = 0; 
    register int i;
    register int np1 = node[p1].number; 
    for (i = 0; i < Number_of_line_elements; i++)
    {
     if ((np1 == element[i].node1) || (np1 == element[i].node2))
     {
      flg = 1; // The point is at the boundary
      break;
     }
    }
  
    if (flg == 1)
    {
     register int domain1 = 0, domain2 = 0;
     register int d;
     for (d = 0; d < Number_of_line_elements; d++)
     { 
      if ((np1 == element[d].node1) || (np1 == element[d].node2))
      {
       domain1 = element[d].physical_domain;
       break;
      }
     }
     register int b; 
     for (b = d+1; b < Number_of_line_elements; b++)
     { 
      if ((np1 == element[b].node1) || (np1 == element[b].node2))
      {
       domain2 = element[b].physical_domain;
       break;
      }
     }
     if ((domain1 == 3 || domain2 == 3) && (domain1 != domain2))
     { 
      flg = 2; // The point is on two different kinds of boundaries, one of which is the wall (solid boundary)
     }      
    }   
    nd[p1].flag = flg;    
  
    if (flg == 0) // If the point is inside the domain all the neighbouring elements have to be considered for extrapolation
    {
     register int Number_of_CCs = 0;     
     register int e;
     for (e = 0; e < Number_of_Elements; e++)
     {
      if ((np1 == element[e].node1) || (np1 == element[e].node2) || (np1 == element[e].node3))
      {     
       Number_of_CCs = Number_of_CCs + 1; 
      }
     }
     nd[p1].n = Number_of_CCs;    
    }    
    else if (flg == 1) // If the point is on the boundary then only those elements are considered that share the boundary
    {
     register int Number_of_CCs = 0;     
     register int e;
     for (e = 0; e < Number_of_Elements; e++)
     {
      if ((np1 == element[e].node1) || (np1 == element[e].node2) || (np1 == element[e].node3))
      {     
       if ((element[e].type == 1) || (element[element[e].nes1 - 1].type == 1) || (element[element[e].nes2 - 1].type == 1) || (element[element[e].nes3 - 1].type == 1))
       {
        Number_of_CCs = Number_of_CCs + 1; 
       }        
      }
     }
     nd[p1].n = Number_of_CCs;    
    }
    else if (flg == 2) // If the point is on the boundary and shares two different boundaries like 1,2 or 2,3 etc.
    {
     register int Number_of_CCs = 0;     
     register int e;
     for (e = 0; e < Number_of_Elements; e++)
     {
      if ((np1 == element[e].node1) || (np1 == element[e].node2) || (np1 == element[e].node3))
      {     
       if ((element[e].type == 1) || (element[element[e].nes1 - 1].type == 1) || (element[element[e].nes2 - 1].type == 1) || (element[element[e].nes3 - 1].type == 1))
       {
        if ((element[e].physical_domain == 3) || (element[element[e].nes1 - 1].physical_domain == 3))
        {
         Number_of_CCs = Number_of_CCs + 1; 
        }          
       }        
      }
     }
     nd[p1].n = Number_of_CCs;    
    }            
   }
  
   // Finding the weights of contributing cell-centers
 
   register int i;
   for (i = 0; i < Number_of_original_Nodes; i++)
   {   
    fprintf(fpnw,"%i %i ", nd[i].flag, nd[i].n);   
    nd[i].cc = malloc(sizeof(Cell_center) * nd[i].n);   
    if (nd[i].flag == 0)
    {
     int j = 0;
     register int e;
     for (e = 0; e < Number_of_Elements; e++)
     {
      if ((node[i].number == element[e].node1) || (node[i].number == element[e].node2) || (node[i].number == element[e].node3))
      {     
       nd[i].cc[j].num = e;
       fprintf(fpnw,"%i ", nd[i].cc[j].num);
       nd[i].cc[j].wv = 1/(sqrt((node[i].x - element[e].cx)*(node[i].x - element[e].cx)+(node[i].y - element[e].cy)*(node[i].y - element[e].cy)));
       fprintf(fpnw,"%lf ", nd[i].cc[j].wv);
       j++;
       if (j == nd[i].n) break; 
      }
     }
    }    
    else if (nd[i].flag == 1)
    {
     int j = 0;
     register int e;
     for (e = 0; e < Number_of_Elements; e++)
     {
      if ((node[i].number == element[e].node1) || (node[i].number == element[e].node2) || (node[i].number == element[e].node3))
      {     
       if ((element[e].type == 1) || (element[element[e].nes1 - 1].type == 1) || (element[element[e].nes2 - 1].type == 1) || (element[element[e].nes3 - 1].type == 1))
       {
        nd[i].cc[j].num = e;
        fprintf(fpnw,"%i ", nd[i].cc[j].num);
        nd[i].cc[j].wv = 1/(sqrt((node[i].x - element[e].cx)*(node[i].x - element[e].cx)+(node[i].y - element[e].cy)*(node[i].y - element[e].cy)));
        fprintf(fpnw,"%lf ", nd[i].cc[j].wv);           
        j++;
        if (j == nd[i].n) break;
       }         
      }
     }
    }
    else if (nd[i].flag == 2)
    {
     int j = 0;
     register int e;
     for (e = 0; e < Number_of_Elements; e++)
     {
      if ((node[i].number == element[e].node1) || (node[i].number == element[e].node2) || (node[i].number == element[e].node3))
      {     
       if ((element[e].type == 1) || (element[element[e].nes1 - 1].type == 1) || (element[element[e].nes2 - 1].type == 1) || (element[element[e].nes3 - 1].type == 1))
       {
        if ((element[e].physical_domain == 3) || (element[element[e].nes1 - 1].physical_domain == 3))
        {          
         nd[i].cc[j].num = e;
         fprintf(fpnw,"%i ", nd[i].cc[j].num);
         nd[i].cc[j].wv = 1/(sqrt((node[i].x - element[e].cx)*(node[i].x - element[e].cx)+(node[i].y - element[e].cy)*(node[i].y - element[e].cy)));
         fprintf(fpnw,"%lf ", nd[i].cc[j].wv);           
         j++;
         if (j == nd[i].n) break;
        }
       }         
      }
     }
    }        
    fprintf(fpnw,"\n");       
   } 
   fclose(fpnw);
  }
  else
  {
   FILE *fpnr = fopen("node.txt","r"); 
   if(!fpnr) 
   {
    perror("Could not find the node.txt file.");
    exit(0); 
   } 
   register int i;
   for (i = 0; i < Number_of_original_Nodes; i++)
   {
    fscanf(fpnr,"%i %i", &nd[i].flag, &nd[i].n);  
    nd[i].cc = malloc(sizeof(Cell_center) * nd[i].n);
    register int j;
    for (j = 0; j < nd[i].n; j++)
    {
     fscanf(fpnr,"%i %lf", &nd[i].cc[j].num, &nd[i].cc[j].wv);
    }   
   } 
   fclose(fpnr);
  }
 }
}
