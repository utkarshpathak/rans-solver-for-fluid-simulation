P = solver
OBJECTS = ReadMeshData.o ReadInputData.o GradientAtCellBoundary.o CalculateMeshGeometryParameters.o CalculateContributingCellCentersToNodeData.o CalculateGreenGaussParameters.o CalculateReconstructionParameters.o CalculateMemoryRequirement.o
CFLAGS = -g -Wall -fopenmp -O3
LDLIBS = -lm
CC = gcc
$(P): $(OBJECTS)

