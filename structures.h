#ifndef STRUCTURES_H_INCLUDED
#define STRUCTURES_H_INCLUDED

typedef struct
{
 double gm;
 int viscous;
 double r_inf;
 double u_inf;
 double v_inf;
 double t_inf;
 double twal;
 double CFL;
 int iterations;
 int readfile;
 int readdata;
 int writetemporaryfile;
 int writingfrequency;
} Input_Data; 

typedef struct
{
 int number; 
 double x;  
 double y; 
 double z; 
} Nodes; 

typedef struct
{
 int number; 
 int type; 
 int domains; 
 int *data; 
} Temp1_Elements; 

typedef struct
{
 int number; 
 int type; 
 int domains; 
 int *data; 
} Temp2_Elements; 

typedef struct // Only some of this data is being used in the actual calculation. So may be divide the structure to suit that.
{
 int number;   
 int type;   
 int domains; 
 int physical_domain;
 int elementary_domain; // can go probably
 int node1;
 int node2;
 int node3; // data till here is basic -- can divide from here may be.
 double cx; // data from here is calculated
 double cy;
 double s;
 double s1;
 double s2;
 double s3;
 double ns1x;
 double ns1y;
 double ns2x;
 double ns2y;
 double ns3x;
 double ns3y;
 int nes1; 
 int nes2;
 int nes3;
 double dl;
} Elements; // This structure is too big. Try to break it into parts then see the speed.

typedef struct
{
 int updates1;
 int updates2;
 int updates3;
 int nes1update;
 int nes2update;
 int nes3update;  
} Elements_Updates;

typedef struct
{
 int num; // Represents the no. of element to which it belongs and this is the number in the element array (not the actual value)
 double wv; // Represents the weight for that cell center 
} Cell_center; // Contributing cell center to the node

typedef struct
{
 int flag; // It is 0 if the node is inside the domain and 1 if the node is at the boudnary
 int n; // No. of contributing cell centers to the node
 Cell_center *cc; // Pointer to the array of contributing cell centers 
} Node_data; // Data of a node

typedef struct
{
 double s1pabx;
 double s1paby;  
 double s1pbcx;
 double s1pbcy;
 double s1pcdx;
 double s1pcdy;
 double s1pdax;
 double s1pday;
 double s2pabx;
 double s2paby;  
 double s2pbcx;
 double s2pbcy;
 double s2pcdx;
 double s2pcdy;
 double s2pdax;
 double s2pday;
 double s3pabx;
 double s3paby;  
 double s3pbcx;
 double s3pbcy;
 double s3pcdx;
 double s3pcdy;
 double s3pdax;
 double s3pday;
} Gradient_Parameters; 

typedef struct
{
 double dx1;
 double dy1;
 double dx2;
 double dy2;
 double dx3;
 double dy3; 
} Reconstruction_Variables; 

typedef struct
{
 int Number_of_original_Nodes; 
 int Number_of_Elements; 
 int Number_of_line_elements;
 int Number_of_Nodes; 
 int Number_of_tri_elements;
 Nodes *node;
 Elements *element;
 Elements_Updates *update_element;
 int (*te) [4];
 Node_data *nd;
 Gradient_Parameters *para;
 Reconstruction_Variables *recon;
} Mesh_Data; 

typedef struct
{
 double r; 
 double ru;  
 double rv; 
 double rE; 
} Conserved_Variables; 

typedef struct
{
 double fr; 
 double fru;  
 double frv; 
 double frE; 
} Interface_Flux;

typedef struct
{
 double r;
 double ru;
 double rv;
 double rE;
} Node_Values;

typedef struct
{
 double r; 
 double u;  
 double v; 
 double t; 
} Primitive_Variables; 

typedef struct
{
 int number;
 int type;
 int node1;
 int node2;
 int node3;
 double cx;
 double cy;
 double ccr;
 double ccu;
 double ccv;
 double cct;
} Unstructured_File_Format;

typedef struct
{
 double pvr;
 double pvu;
 double pvv;
 double pvt;
} Point_Values;

#endif
