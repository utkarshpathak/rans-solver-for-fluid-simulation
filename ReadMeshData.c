#include "solver.h"

static void GetNumberofOriginalNodesNumberofElements(Mesh_Data *MeshData);

static void GetNumberofLineElements(Mesh_Data *MeshData);

static void CreateNodes(Mesh_Data *MeshData);

static void GetNodeData(Mesh_Data *MeshData);

static void GetNodeDataofTriangularElements(Mesh_Data *MeshData);

static void CreateElements(Mesh_Data *MeshData);

static void CreateDummyCells(Mesh_Data *MeshData);

void ReadMeshData(Mesh_Data *MeshData)
{
 GetNumberofOriginalNodesNumberofElements(MeshData);
 GetNumberofLineElements(MeshData);
 (*MeshData).Number_of_Nodes = (*MeshData).Number_of_original_Nodes + (*MeshData).Number_of_line_elements; 
 CreateNodes(MeshData);
 GetNodeData(MeshData); 
 GetNodeDataofTriangularElements(MeshData);
 CreateElements(MeshData);
 CreateDummyCells(MeshData); 
}









//----------------------------------------------------------------------------------------------------------------------------------------------

static void GetNumberofOriginalNodesNumberofElements(Mesh_Data *MeshData)
{
 // Find the number of original nodes and number of elements  
 
 char line[200];
 FILE *fp1 = fopen("flow.msh","r"); 
 if(!fp1) 
 {
  perror("Could not find the file");
  exit(0); 
 }
 while(fgets(line,200,fp1) != NULL) 
 {
  if(strstr(line,"$Nodes")) 
  {
   fscanf(fp1,"%i",&((*MeshData).Number_of_original_Nodes)); 
  } 
  if(strstr(line,"$Elements")) 
  {
   fscanf(fp1,"%i ",&((*MeshData).Number_of_Elements)); 
   break;
  }
 }  
 fclose(fp1); 
}

//----------------------------------------------------------------------------------------------------------------------------------------------

static void GetNumberofLineElements(Mesh_Data *MeshData)
{
 // Use the data of Number of elements to get the data - number of only line elements (to be used for creation of dummy elements)
 
 int Number_of_Elements = (*MeshData).Number_of_Elements;
 int Number_of_line_elements = 0;
 
 char line[200];
 Temp1_Elements *temp1_element = malloc(sizeof(Temp1_Elements) * Number_of_Elements);  
 if (temp1_element == NULL)
 {
  printf("Could not declare temporary elements. Out of memory.\n");
  exit(1);
 }

 FILE *fp2 = fopen("flow.msh","r"); 
 if(!fp2) 
 {
  perror("Could not find the file");
  exit(0); 
 }
 while(fgets(line,200,fp2) != NULL) 
 {  
  if(strstr(line,"$Elements")) 
  {
   fscanf(fp2,"%i ",&Number_of_Elements);  
   register int i;
   for(i = 0; i < Number_of_Elements; i++) 
   {     
    fscanf(fp2, "%i %i %i", &temp1_element[i].number, &temp1_element[i].type, &temp1_element[i].domains);        
    if (temp1_element[i].type == 1)
    {
     Number_of_line_elements = Number_of_line_elements + 1;
    }               
    temp1_element[i].data = malloc(sizeof(int) * (temp1_element[i].type + 3));        
    register int j;
    for (j=0; j < (temp1_element[i].type + 3) ; j++) 
    {
     fscanf(fp2, "%i", temp1_element[i].data);            
    }           
    free(temp1_element[i].data); 
   }      
  }
 }
 (*MeshData).Number_of_line_elements = Number_of_line_elements;  
 fclose(fp2);
 free(temp1_element);  
}

//----------------------------------------------------------------------------------------------------------------------------------------------

static void CreateNodes(Mesh_Data *MeshData)
{
 (*MeshData).node = malloc(sizeof(Nodes) * (*MeshData).Number_of_Nodes); 
 if ((*MeshData).node == NULL)
 {
  printf("Could not declare nodes. Out of memory.\n");
  exit(1);
 }
}

//----------------------------------------------------------------------------------------------------------------------------------------------

static void GetNodeData(Mesh_Data *MeshData)
{ 
 // Get the data of the nodes

 Nodes *node = (*MeshData).node;
 int Number_of_original_Nodes = (*MeshData).Number_of_original_Nodes; 
 
 char line[200];
 FILE *fp3 = fopen("flow.msh","r"); 
 if(!fp3) 
 {
  perror("Could not find the file");
  exit(0); 
 }
 while(fgets(line,200,fp3) != NULL) 
 {
  if(strstr(line,"$Nodes")) 
  {
   fscanf(fp3,"%i",&Number_of_original_Nodes);      
   register int i;
   for(i = 0; i < Number_of_original_Nodes; i++) 
   {     
    fscanf(fp3, "%i %lf %lf %lf", &node[i].number, &node[i].x, &node[i].y, &node[i].z);         
   } 
   break;  
  }
 }
fclose(fp3);
}

//----------------------------------------------------------------------------------------------------------------------------------------------

static void GetNodeDataofTriangularElements(Mesh_Data *MeshData)
{
 // Get the array for the data of nodes of all the triangular elements only

 (*MeshData).Number_of_tri_elements = (*MeshData).Number_of_Elements - (*MeshData).Number_of_line_elements;

 int Number_of_line_elements = (*MeshData).Number_of_line_elements;
 int Number_of_tri_elements = (*MeshData).Number_of_tri_elements;
 int Number_of_Elements = (*MeshData).Number_of_Elements;
 
 int (*tri_ele) [6] = malloc(sizeof(int) * Number_of_tri_elements * 6);   
 
 Temp2_Elements *temp2_element = malloc(sizeof(Temp2_Elements) * (*MeshData).Number_of_Elements); 
 if (temp2_element == NULL)
 {
  printf("Could not declare temporary elements. Out of memory.\n");
  exit(1);
 }

 char line[200]; 
 FILE *fp4 = fopen("flow.msh","r"); 
 if(!fp4) 
 {
  perror("Could not find the file");
  exit(0); 
 }
 while(fgets(line,200,fp4) != NULL) 
 {  
  if(strstr(line,"$Elements")) 
  {
   fscanf(fp4,"%i ",&Number_of_Elements);  
   register int i;
   for(i = 0; i < Number_of_Elements; i++) 
   {     
    fscanf(fp4, "%i %i %i", &temp2_element[i].number, &temp2_element[i].type, &temp2_element[i].domains);          
    temp2_element[i].data = malloc(sizeof(int) * (temp2_element[i].type + 3));        
    register int j;
    for (j=0; j < (temp2_element[i].type + 3) ; j++) 
    {
     fscanf(fp4, "%i", temp2_element[i].data); 
     if (temp2_element[i].type == 2)
     {            
      tri_ele [i - Number_of_line_elements] [0] = temp2_element[i].number;     
      tri_ele [i - Number_of_line_elements] [j+1] = *temp2_element[i].data; 
     }           
    }  
    free(temp2_element[i].data); 
   }      
  }
 }
 fclose(fp4);
 free(temp2_element);
 
 // Data for all triangular elements
 
 (*MeshData).te = malloc(sizeof(int) * Number_of_Elements * 4);   
 int (*te) [4] = (*MeshData).te; 
 register int m;
 for (m = 0; m < Number_of_tri_elements; m++)
 {
  te[m][0] = tri_ele[m][0]; // number
  te[m][1] = tri_ele[m][3]; // node1
  te[m][2] = tri_ele[m][4]; // node2
  te[m][3] = tri_ele[m][5]; // node3
 } 
 free(tri_ele);
}

//----------------------------------------------------------------------------------------------------------------------------------------------

static void CreateElements(Mesh_Data *MeshData)
{
 (*MeshData).element = malloc(sizeof(Elements) * (*MeshData).Number_of_Elements); 
 if ((*MeshData).element == NULL)
 {
  printf("Could not declare elements. Out of memory.\n");
  exit(1);
 } 
}

//----------------------------------------------------------------------------------------------------------------------------------------------

static void CreateDummyCells(Mesh_Data *MeshData)
{
 // Creation of dummy cells i.e. dummy points
 
 register int Number_of_original_Nodes = (*MeshData).Number_of_original_Nodes; 
 int Number_of_Elements = (*MeshData).Number_of_Elements; 
 register int Number_of_tri_elements = (*MeshData).Number_of_tri_elements;
 Elements *element = (*MeshData).element;
 Nodes *node = (*MeshData).node;
 int (*te) [4] = (*MeshData).te;
  
 char line[200];
 FILE *fp5 = fopen("flow.msh","r"); 
 if(!fp5) 
 {
  perror("Could not find the file");
  exit(0); 
 }
 while(fgets(line,200,fp5) != NULL) 
 {  
  if(strstr(line,"$Elements")) 
  {
   fscanf(fp5,"%i ",&Number_of_Elements);  
   register int i;
   for(i = 0; i < Number_of_Elements; i++) 
   {     
    fscanf(fp5, "%i %i %i %i %i %i %i", &element[i].number, &element[i].type, &element[i].domains, &element[i].physical_domain, &element[i].elementary_domain, &element[i].node1, &element[i].node2); 
    if (element[i].type == 1)
    {
     element[i].node3 = Number_of_original_Nodes + 1 + i;
     node[Number_of_original_Nodes + i].number = Number_of_original_Nodes + 1 + i;
     register int sort, node_no_in_array;
     register double a, b;
     for (sort = 0; sort < Number_of_tri_elements; sort++)
     {
      if ((te[sort][1]==element[i].node1 || te[sort][2]==element[i].node1 || te[sort][3]==element[i].node1) && (te[sort][1]==element[i].node2 || te[sort][2]==element[i].node2 || te[sort][3]==element[i].node2))
      {
       if ((te[sort][1] != element[i].node1) && (te[sort][1] != element[i].node2))
       {
        node_no_in_array = te[sort][1] - 1;
        a = node[node_no_in_array].x;
        b = node[node_no_in_array].y;
        break;
       }
       else if ((te[sort][2] != element[i].node1) && (te[sort][2] != element[i].node2))
       {
        node_no_in_array = te[sort][2] - 1;
        a = node[node_no_in_array].x;
        b = node[node_no_in_array].y;
        break;
       } 
       else 
       {
        node_no_in_array = te[sort][3] - 1;
        a = node[node_no_in_array].x;
        b = node[node_no_in_array].y;
        break;
       }
      }
     }
     register double x1, y1, x2, y2;
     x1 = node[element[i].node1 -1].x;
     y1 = node[element[i].node1 -1].y;
     x2 = node[element[i].node2 -1].x;
     y2 = node[element[i].node2 -1].y;
     register double m1, m2, m3, m4;
     m1 = a - x1;
     m2 = b - y1;
     m3 = x2 - x1;
     m4 = y2 -y1;
     node[Number_of_original_Nodes + i].x = (2/(m3*m3+m4*m4))*(m1*m3*m3+m3*m4*m2) - m1 + x1;
     node[Number_of_original_Nodes + i].y = (2/(m3*m3+m4*m4))*(m1*m3*m4+m2*m4*m4) - m2 + y1;
     node[Number_of_original_Nodes + i].z = 0;
    }
    else
    {
     fscanf(fp5, "%i", &element[i].node3);
    }
   }      
  }
 }
fclose(fp5);

register int init;
for (init=0; init < Number_of_Elements; init++) 
{
 te[init][0]= element[init].number;
 te[init][1]= element[init].node1;
 te[init][2]= element[init].node2;
 te[init][3]= element[init].node3;
}
}

