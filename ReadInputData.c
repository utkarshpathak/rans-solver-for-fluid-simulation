#include "solver.h"

void ReadInputData(Input_Data *InputData)
{
 
printf("\nReading input data for simulation:\n\n");

char line[200];

FILE *fp0 = fopen("flow.inp","r"); 
if(!fp0) 
 {
  perror("Could not find the file");
  exit(0); 
 }
while(fgets(line,200,fp0) != NULL) 
  {
   if(strstr(line,"Gamma")) 
    {
     fscanf(fp0,"%lf",&((*InputData).gm)); 
     printf("Ratio of specific heats is %lf\n", (*InputData).gm);
    }
   if(strstr(line,"Viscous")) 
    {
     fscanf(fp0,"%i",&((*InputData).viscous));
     if ((*InputData).viscous != 0)
      {
       (*InputData).viscous = 1;
       printf("Viscous simulation.\n");
      }
     else
      {
       printf("Inviscid simulation.\n");
      }  
    }  
   if(strstr(line,"Density")) 
    {
     fscanf(fp0,"%lf",&((*InputData).r_inf)); 
     printf("Freestream density is %lf\n", (*InputData).r_inf);     
    }
   if(strstr(line,"X_Velocity")) 
    {
     fscanf(fp0,"%lf",&((*InputData).u_inf)); 
     printf("Freestream X velocity is %lf\n", (*InputData).u_inf);     
    }
   if(strstr(line,"Y_Velocity")) 
    {
     fscanf(fp0,"%lf",&((*InputData).v_inf)); 
     printf("Freestream Y velocity is %lf\n", (*InputData).v_inf);      
    } 
   if(strstr(line,"Temperature")) 
    {
     fscanf(fp0,"%lf",&((*InputData).t_inf)); 
     printf("Freestream temperature is %lf\n", (*InputData).t_inf);      
    } 
   if(strstr(line,"WallTemp")) 
    {
     fscanf(fp0,"%lf",&((*InputData).twal)); 
     printf("Wall temperature is: %lf\n", (*InputData).twal);
    } 
   if(strstr(line,"CFL")) 
    {
     fscanf(fp0,"%lf",&((*InputData).CFL)); 
     printf("Courant–Friedrichs–Lewy number is %lf\n", (*InputData).CFL);      
    }
   if(strstr(line,"Iterations")) 
    {
     fscanf(fp0,"%i",&((*InputData).iterations)); 
     printf("Total number of iterations to be carried out: %i\n", (*InputData).iterations);     
    }
   if(strstr(line,"Readfile")) 
    {
     fscanf(fp0,"%i",&((*InputData).readfile)); 
     if ((*InputData).readfile != 0)
      {
       printf("Will read file from a previous simulation.\n");
      }
     else
      {
       printf("New simulation.\n");
      }     
    }
   if(strstr(line,"Readdata")) 
    {
     fscanf(fp0,"%i",&((*InputData).readdata)); 
     if ((*InputData).readdata != 0)
      {
       printf("Will read parameters from calc.txt, ne.txt and node.txt.\n");
      }
     else
      {
       printf("Will generate calc.txt, ne.txt and node.txt.\n");
      }     
    } 
   if(strstr(line,"Writetemporaryfile")) 
    {
     fscanf(fp0,"%i",&((*InputData).writetemporaryfile)); 
     if ((*InputData).writetemporaryfile != 0)
      {
       printf("Will write temporary files.\n");
      }
     else
      {
       printf("No temporary files will be written.\n");
      }     
    }
   if(strstr(line,"Writingfrequency")) 
    {
     fscanf(fp0,"%i",&((*InputData).writingfrequency)); 
     if ((*InputData).writingfrequency == 0)
      {
       printf("Writing frequency cannot be zero. Exiting the simulation.\n");
       exit(1);
      }
    }                                      
  }  
fclose(fp0);

printf("\n");
}
