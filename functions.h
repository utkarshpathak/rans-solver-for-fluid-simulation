#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

void GradientAtCellBoundary(int s, int element_num, double *variable_array, Conserved_Variables *x, Gradient_Parameters *p, Node_Values *nv, Elements *ele);

double GradientAtCellCenter(int eq, int dir, int z, Conserved_Variables *x, Node_data *ndt, Elements *ele); 

void ReadInputData(Input_Data *InputData);

void ReadMeshData(Mesh_Data *MeshData);

void CalculateMeshGeometryParameters(Input_Data *InputData, Mesh_Data *MeshData);

void CalculateContributingCellCentersToNodeData(Input_Data *InputData, Mesh_Data *MeshData);

void CalculateGreenGaussParameters(Input_Data *InputData, Mesh_Data *MeshData);

void CalculateReconstructionParameters(Input_Data *InputData, Mesh_Data *MeshData);

void CalculateMemoryRequirement(Input_Data *InputData, Mesh_Data *MeshData);

#endif
