#include "solver.h"

// Function to find the gradient of any quantity at a specific side of a given element.

// z = element no., s = side no., dir = direction of gradient, *x = *U, *p = *para, *ndt = *nd, *ele = *element
 
void GradientAtCellBoundary(int s, int element_num, double *variable_array, Conserved_Variables *x, Gradient_Parameters *p, Node_Values *nv, Elements *ele)
{
   
 // Comment for future : na,nc,i,j can be declared with register storage class as its used regularly. Put whole thing in {}.  
   
 double gm = 1.4;
 register double cv = 287.1/(gm - 1);
 register double phia_u, phib_u, phic_u, phid_u;
 register double phia_v, phib_v, phic_v, phid_v;
 register double phia_t, phib_t, phic_t, phid_t;
 register double abx, aby, bcx, bcy, cdx, cdy, dax, day;
 register int ne; // neighbouring element number in the arrays (not the actual number)
 register int na; // node number for point a (in the arrays)
 register int nc; // node number for point c (in the arrays)
 register int z = element_num;

 // Declaring green-gauss parameters, finding the neighbouring element no. and nodes for the correct side. 

 if (s == 1)
  {
   abx = p[z].s1pabx;
   aby = p[z].s1paby;
   bcx = p[z].s1pbcx;
   bcy = p[z].s1pbcy;
   cdx = p[z].s1pcdx;
   cdy = p[z].s1pcdy;
   dax = p[z].s1pdax;
   day = p[z].s1pday;
   ne = ele[z].nes1 - 1;
   na = ele[z].node1 - 1;
   nc = ele[z].node2 - 1;
  }
 else if (s == 2)
  {
   abx = p[z].s2pabx;
   aby = p[z].s2paby;
   bcx = p[z].s2pbcx;
   bcy = p[z].s2pbcy;
   cdx = p[z].s2pcdx;
   cdy = p[z].s2pcdy;
   dax = p[z].s2pdax;
   day = p[z].s2pday;
   ne = ele[z].nes2 - 1;
   na = ele[z].node2 - 1;
   nc = ele[z].node3 - 1;
  } 
 else if (s == 3)
  {
   abx = p[z].s3pabx;
   aby = p[z].s3paby;
   bcx = p[z].s3pbcx;
   bcy = p[z].s3pbcy;
   cdx = p[z].s3pcdx;
   cdy = p[z].s3pcdy;
   dax = p[z].s3pdax;
   day = p[z].s3pday;
   ne = ele[z].nes3 - 1;
   na = ele[z].node3 - 1;
   nc = ele[z].node1 - 1;
  }
 
 register double rho = x[z].r;
 register double u = x[z].ru / rho;
 register double v = x[z].rv / rho;

 phib_u = u;
 phib_v = v;
 phib_t = ((x[z].rE/rho) - 0.5 * ((u * u) + (v * v))) / cv;

 rho = x[ne].r;
 u = x[ne].ru / rho;
 v = x[ne].rv / rho;

 phid_u = u;
 phid_v = v;
 phid_t = ((x[ne].rE/rho) - 0.5 * ((u * u) + (v * v))) / cv;
          
 rho = nv[na].r;
 u = nv[na].ru / rho;
 v = nv[na].rv / rho;          
          
 phia_u = u;
 phia_v = v;
 phia_t = ((nv[na].rE/rho) - 0.5 * ((u * u) + (v * v))) / cv;
  
 rho = nv[nc].r;
 u = nv[nc].ru / rho;
 v = nv[nc].rv / rho;          
          
 phic_u = u;
 phic_v = v;
 phic_t = ((nv[nc].rE/rho) - 0.5 * ((u * u) + (v * v))) / cv;
 

 variable_array[0] = (phia_u + phib_u) * abx + (phib_u + phic_u) * bcx + (phic_u + phid_u) * cdx + (phid_u + phia_u) * dax; 
 variable_array[1] = (phia_u + phib_u) * aby + (phib_u + phic_u) * bcy + (phic_u + phid_u) * cdy + (phid_u + phia_u) * day;  
 variable_array[2] = (phia_v + phib_v) * abx + (phib_v + phic_v) * bcx + (phic_v + phid_v) * cdx + (phid_v + phia_v) * dax; 
 variable_array[3] = (phia_v + phib_v) * aby + (phib_v + phic_v) * bcy + (phic_v + phid_v) * cdy + (phid_v + phia_v) * day;    
 variable_array[4] = (phia_t + phib_t) * abx + (phib_t + phic_t) * bcx + (phic_t + phid_t) * cdx + (phid_t + phia_t) * dax; 
 variable_array[5] = (phia_t + phib_t) * aby + (phib_t + phic_t) * bcy + (phic_t + phid_t) * cdy + (phid_t + phia_t) * day;     
}
