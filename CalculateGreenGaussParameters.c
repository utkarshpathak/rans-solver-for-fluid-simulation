#include "solver.h"

void CalculateGreenGaussParameters(Input_Data *InputData, Mesh_Data *MeshData)
{
 // Calculation of Green-Gauss parameters 
 
 Nodes *node = (*MeshData).node;
 Elements *element = (*MeshData).element;
 register int Number_of_Elements = (*MeshData).Number_of_Elements;  
 
 (*MeshData).para = malloc(sizeof(Gradient_Parameters) * Number_of_Elements * (*InputData).viscous);
 Gradient_Parameters *para = (*MeshData).para; 
 if (((*InputData).viscous != 0) && (para == NULL))
 {
  printf("Could not declare gradient parameters. Out of memory.\n");
  exit(1);
 }

 if ((*InputData).viscous != 0)
 {  
  register int gg;
  for (gg = 0; gg < Number_of_Elements; gg++)
  {  
   if(element[gg].nes1 == 0)
   {
    para[gg].s1pabx = 0;
    para[gg].s1paby = 0;
    para[gg].s1pbcx = 0;
    para[gg].s1pbcy = 0;
    para[gg].s1pcdx = 0;
    para[gg].s1pcdy = 0;
    para[gg].s1pdax = 0;
    para[gg].s1pday = 0;
   }
   else 
   {
    register double ggs1ax, ggs1ay, ggs1bx, ggs1by, ggs1cx, ggs1cy, ggs1dx, ggs1dy; 
    double ggs1abcd, ggs1lab, ggs1lbc, ggs1lcd, ggs1lda, ggs1nabx, ggs1naby, ggs1nbcx, ggs1nbcy, ggs1ncdx, ggs1ncdy, ggs1ndax, ggs1nday;
 
    ggs1ax = node[element[gg].node1 - 1].x;
    ggs1ay = node[element[gg].node1 - 1].y;
    ggs1bx = element[gg].cx;
    ggs1by = element[gg].cy;
    ggs1cx = node[element[gg].node2 - 1].x;
    ggs1cy = node[element[gg].node2 - 1].y;
    ggs1dx = element[element[gg].nes1 - 1].cx;
    ggs1dy = element[element[gg].nes1 - 1].cy;
 
    double ggs1abcd1, ggs1abcd2;
    ggs1abcd1 = (ggs1bx*ggs1cy - ggs1bx*ggs1ay - ggs1by*ggs1cx + ggs1by*ggs1ax + ggs1cx*ggs1ay - ggs1ax*ggs1cy)/2; 
    ggs1abcd2 = (ggs1dx*ggs1ay - ggs1dx*ggs1cy - ggs1dy*ggs1ax + ggs1dy*ggs1cx + ggs1ax*ggs1cy - ggs1cx*ggs1ay)/2;
    ggs1abcd = fabs(ggs1abcd1) + fabs(ggs1abcd2);
 
    register double twice_ggs1abcd_inverse = 1/(2*ggs1abcd);
 
    ggs1lab = sqrt((ggs1bx - ggs1ax)*(ggs1bx - ggs1ax) + (ggs1by - ggs1ay)*(ggs1by - ggs1ay)); 
    ggs1lbc = sqrt((ggs1cx - ggs1bx)*(ggs1cx - ggs1bx) + (ggs1cy - ggs1by)*(ggs1cy - ggs1by)); 
    ggs1lcd = sqrt((ggs1dx - ggs1cx)*(ggs1dx - ggs1cx) + (ggs1dy - ggs1cy)*(ggs1dy - ggs1cy)); 
    ggs1lda = sqrt((ggs1ax - ggs1dx)*(ggs1ax - ggs1dx) + (ggs1ay - ggs1dy)*(ggs1ay - ggs1dy)); 
 
    double a, b;
    a = ggs1dx;
    b = ggs1dy;
 
    register double m1, m2, m3, m4;
    m1 = ggs1bx - ggs1ax;
    m2 = ggs1by - ggs1ay;
    m3 = ggs1bx - a;
    m4 = ggs1by - b;
 
    register double c, d;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs1nabx = c/(sqrt(c*c + d*d));
    ggs1naby = d/(sqrt(c*c + d*d));
    a = ggs1dx;
    b = ggs1dy;
    m1 = ggs1cx - ggs1bx;
    m2 = ggs1cy - ggs1by;
    m3 = ggs1cx - a;
    m4 = ggs1cy - b;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs1nbcx = c/(sqrt(c*c + d*d));
    ggs1nbcy = d/(sqrt(c*c + d*d));
    a = ggs1bx;
    b = ggs1by;
    m1 = ggs1dx - ggs1cx;
    m2 = ggs1dy - ggs1cy;
    m3 = ggs1dx - a;
    m4 = ggs1dy - b;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs1ncdx = c/(sqrt(c*c + d*d));
    ggs1ncdy = d/(sqrt(c*c + d*d));
    a = ggs1bx;
    b = ggs1by;
    m1 = ggs1ax - ggs1dx;
    m2 = ggs1ay - ggs1dy;
    m3 = ggs1ax - a;
    m4 = ggs1ay - b;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs1ndax = c/(sqrt(c*c + d*d));
    ggs1nday = d/(sqrt(c*c + d*d));
 
    para[gg].s1pabx = (ggs1nabx*ggs1lab)*twice_ggs1abcd_inverse; 
    para[gg].s1paby = (ggs1naby*ggs1lab)*twice_ggs1abcd_inverse;
    para[gg].s1pbcx = (ggs1nbcx*ggs1lbc)*twice_ggs1abcd_inverse;
    para[gg].s1pbcy = (ggs1nbcy*ggs1lbc)*twice_ggs1abcd_inverse;
    para[gg].s1pcdx = (ggs1ncdx*ggs1lcd)*twice_ggs1abcd_inverse;
    para[gg].s1pcdy = (ggs1ncdy*ggs1lcd)*twice_ggs1abcd_inverse;
    para[gg].s1pdax = (ggs1ndax*ggs1lda)*twice_ggs1abcd_inverse;
    para[gg].s1pday = (ggs1nday*ggs1lda)*twice_ggs1abcd_inverse; 
   }
 
   if(element[gg].nes2 == 0)
   {
    para[gg].s2pabx = 0;
    para[gg].s2paby = 0;
    para[gg].s2pbcx = 0;
    para[gg].s2pbcy = 0;
    para[gg].s2pcdx = 0;
    para[gg].s2pcdy = 0;
    para[gg].s2pdax = 0;
    para[gg].s2pday = 0;
   }
   else 
   {
    register double ggs2ax, ggs2ay, ggs2bx, ggs2by, ggs2cx, ggs2cy, ggs2dx, ggs2dy; 
    double ggs2abcd, ggs2lab, ggs2lbc, ggs2lcd, ggs2lda, ggs2nabx, ggs2naby, ggs2nbcx, ggs2nbcy, ggs2ncdx, ggs2ncdy, ggs2ndax, ggs2nday;
 
    ggs2ax = node[element[gg].node2 - 1].x;
    ggs2ay = node[element[gg].node2 - 1].y;
    ggs2bx = element[gg].cx;
    ggs2by = element[gg].cy;
    ggs2cx = node[element[gg].node3 - 1].x;
    ggs2cy = node[element[gg].node3 - 1].y;
    ggs2dx = element[element[gg].nes2 - 1].cx;
    ggs2dy = element[element[gg].nes2 - 1].cy;
 
    double ggs2abcd1, ggs2abcd2;
    ggs2abcd1 = (ggs2bx*ggs2cy - ggs2bx*ggs2ay - ggs2by*ggs2cx + ggs2by*ggs2ax + ggs2cx*ggs2ay - ggs2ax*ggs2cy)/2; 
    ggs2abcd2 = (ggs2dx*ggs2ay - ggs2dx*ggs2cy - ggs2dy*ggs2ax + ggs2dy*ggs2cx + ggs2ax*ggs2cy - ggs2cx*ggs2ay)/2;
    ggs2abcd = fabs(ggs2abcd1) + fabs(ggs2abcd2);  
 
    register double twice_ggs2abcd_inverse = 1/(2*ggs2abcd);
 
    ggs2lab = sqrt((ggs2bx - ggs2ax)*(ggs2bx - ggs2ax) + (ggs2by - ggs2ay)*(ggs2by - ggs2ay)); 
    ggs2lbc = sqrt((ggs2cx - ggs2bx)*(ggs2cx - ggs2bx) + (ggs2cy - ggs2by)*(ggs2cy - ggs2by)); 
    ggs2lcd = sqrt((ggs2dx - ggs2cx)*(ggs2dx - ggs2cx) + (ggs2dy - ggs2cy)*(ggs2dy - ggs2cy)); 
    ggs2lda = sqrt((ggs2ax - ggs2dx)*(ggs2ax - ggs2dx) + (ggs2ay - ggs2dy)*(ggs2ay - ggs2dy)); 
 
    double a, b;
    a = ggs2dx;
    b = ggs2dy;
 
    register double m1, m2, m3, m4;
    m1 = ggs2bx - ggs2ax;
    m2 = ggs2by - ggs2ay;
    m3 = ggs2bx - a;
    m4 = ggs2by - b;
 
    register double c, d;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs2nabx = c/(sqrt(c*c + d*d));
    ggs2naby = d/(sqrt(c*c + d*d));
    a = ggs2dx;
    b = ggs2dy;
    m1 = ggs2cx - ggs2bx;
    m2 = ggs2cy - ggs2by;
    m3 = ggs2cx - a;
    m4 = ggs2cy - b;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs2nbcx = c/(sqrt(c*c + d*d));
    ggs2nbcy = d/(sqrt(c*c + d*d));
    a = ggs2bx;
    b = ggs2by;
    m1 = ggs2dx - ggs2cx;
    m2 = ggs2dy - ggs2cy;
    m3 = ggs2dx - a;
    m4 = ggs2dy - b;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs2ncdx = c/(sqrt(c*c + d*d));
    ggs2ncdy = d/(sqrt(c*c + d*d));
    a = ggs2bx;
    b = ggs2by;
    m1 = ggs2ax - ggs2dx;
    m2 = ggs2ay - ggs2dy;
    m3 = ggs2ax - a;
    m4 = ggs2ay - b;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs2ndax = c/(sqrt(c*c + d*d));
    ggs2nday = d/(sqrt(c*c + d*d));
 
    para[gg].s2pabx = (ggs2nabx*ggs2lab)*twice_ggs2abcd_inverse;
    para[gg].s2paby = (ggs2naby*ggs2lab)*twice_ggs2abcd_inverse;
    para[gg].s2pbcx = (ggs2nbcx*ggs2lbc)*twice_ggs2abcd_inverse;
    para[gg].s2pbcy = (ggs2nbcy*ggs2lbc)*twice_ggs2abcd_inverse;
    para[gg].s2pcdx = (ggs2ncdx*ggs2lcd)*twice_ggs2abcd_inverse;
    para[gg].s2pcdy = (ggs2ncdy*ggs2lcd)*twice_ggs2abcd_inverse;
    para[gg].s2pdax = (ggs2ndax*ggs2lda)*twice_ggs2abcd_inverse;
    para[gg].s2pday = (ggs2nday*ggs2lda)*twice_ggs2abcd_inverse;
   }
 
   if(element[gg].nes3 == 0)
   {
    para[gg].s3pabx = 0;
    para[gg].s3paby = 0;
    para[gg].s3pbcx = 0;
    para[gg].s3pbcy = 0;
    para[gg].s3pcdx = 0;
    para[gg].s3pcdy = 0;
    para[gg].s3pdax = 0;
    para[gg].s3pday = 0;
   }
   else 
   {
    register double ggs3ax, ggs3ay, ggs3bx, ggs3by, ggs3cx, ggs3cy, ggs3dx, ggs3dy; 
    double ggs3abcd, ggs3lab, ggs3lbc, ggs3lcd, ggs3lda, ggs3nabx, ggs3naby, ggs3nbcx, ggs3nbcy, ggs3ncdx, ggs3ncdy, ggs3ndax, ggs3nday;
  
    ggs3ax = node[element[gg].node3 - 1].x;
    ggs3ay = node[element[gg].node3 - 1].y;
    ggs3bx = element[gg].cx;
    ggs3by = element[gg].cy;
    ggs3cx = node[element[gg].node1 - 1].x;
    ggs3cy = node[element[gg].node1 - 1].y;
    ggs3dx = element[element[gg].nes3 - 1].cx;
    ggs3dy = element[element[gg].nes3 - 1].cy;
 
    double ggs3abcd1, ggs3abcd2;
    ggs3abcd1 = (ggs3bx*ggs3cy - ggs3bx*ggs3ay - ggs3by*ggs3cx + ggs3by*ggs3ax + ggs3cx*ggs3ay - ggs3ax*ggs3cy)/2; 
    ggs3abcd2 = (ggs3dx*ggs3ay - ggs3dx*ggs3cy - ggs3dy*ggs3ax + ggs3dy*ggs3cx + ggs3ax*ggs3cy - ggs3cx*ggs3ay)/2;
    ggs3abcd = fabs(ggs3abcd1) + fabs(ggs3abcd2);
 
    register double twice_ggs3abcd_inverse = 1/(2*ggs3abcd); 
 
    ggs3lab = sqrt((ggs3bx - ggs3ax)*(ggs3bx - ggs3ax) + (ggs3by - ggs3ay)*(ggs3by - ggs3ay)); 
    ggs3lbc = sqrt((ggs3cx - ggs3bx)*(ggs3cx - ggs3bx) + (ggs3cy - ggs3by)*(ggs3cy - ggs3by)); 
    ggs3lcd = sqrt((ggs3dx - ggs3cx)*(ggs3dx - ggs3cx) + (ggs3dy - ggs3cy)*(ggs3dy - ggs3cy)); 
    ggs3lda = sqrt((ggs3ax - ggs3dx)*(ggs3ax - ggs3dx) + (ggs3ay - ggs3dy)*(ggs3ay - ggs3dy)); 
 
    double a, b;
    a = ggs3dx;
    b = ggs3dy;
 
    register double m1, m2, m3, m4;
    m1 = ggs3bx - ggs3ax;
    m2 = ggs3by - ggs3ay;
    m3 = ggs3bx - a;
    m4 = ggs3by - b;
 
    register double c, d;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs3nabx = c/(sqrt(c*c + d*d));
    ggs3naby = d/(sqrt(c*c + d*d));
    a = ggs3dx;
    b = ggs3dy;
    m1 = ggs3cx - ggs3bx;
    m2 = ggs3cy - ggs3by;
    m3 = ggs3cx - a;
    m4 = ggs3cy - b;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs3nbcx = c/(sqrt(c*c + d*d));
    ggs3nbcy = d/(sqrt(c*c + d*d));
    a = ggs3bx;
    b = ggs3by;
    m1 = ggs3dx - ggs3cx;
    m2 = ggs3dy - ggs3cy;
    m3 = ggs3dx - a;
    m4 = ggs3dy - b;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs3ncdx = c/(sqrt(c*c + d*d));
    ggs3ncdy = d/(sqrt(c*c + d*d));
    a = ggs3bx;
    b = ggs3by;
    m1 = ggs3ax - ggs3dx;
    m2 = ggs3ay - ggs3dy;
    m3 = ggs3ax - a;
    m4 = ggs3ay - b;
    c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
    d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
    ggs3ndax = c/(sqrt(c*c + d*d));
    ggs3nday = d/(sqrt(c*c + d*d));
 
    para[gg].s3pabx = (ggs3nabx*ggs3lab)*twice_ggs3abcd_inverse;
    para[gg].s3paby = (ggs3naby*ggs3lab)*twice_ggs3abcd_inverse;
    para[gg].s3pbcx = (ggs3nbcx*ggs3lbc)*twice_ggs3abcd_inverse;
    para[gg].s3pbcy = (ggs3nbcy*ggs3lbc)*twice_ggs3abcd_inverse;
    para[gg].s3pcdx = (ggs3ncdx*ggs3lcd)*twice_ggs3abcd_inverse;
    para[gg].s3pcdy = (ggs3ncdy*ggs3lcd)*twice_ggs3abcd_inverse;
    para[gg].s3pdax = (ggs3ndax*ggs3lda)*twice_ggs3abcd_inverse;
    para[gg].s3pday = (ggs3nday*ggs3lda)*twice_ggs3abcd_inverse;
   }
  }
 } 
}
