#include "solver.h"

void CalculateMeshGeometryParameters(Input_Data *InputData, Mesh_Data *MeshData)
{ 
 int Number_of_Elements = (*MeshData).Number_of_Elements; 
 Nodes *node = (*MeshData).node;
 Elements *element = (*MeshData).element;
 int (*te) [4] = (*MeshData).te; // (*MeshData).te is freed in this function. 

 if (((*InputData).readfile == 0) && ((*InputData).readdata == 0))
 {
  FILE *fpcw;
  fpcw = fopen("calc.txt", "w");
  if (fpcw == NULL)
  {
   printf("File calc.txt could not be generated.\n");
  }

  register int m; 
  for (m = 0; m < Number_of_Elements; m++) 
  {
   register int node1 = element[m].node1;
   register int node2 = element[m].node2;
   register int node3 = element[m].node3;      
   
   register double x1, y1, x2, y2, x3, y3; 
   x1 = node[node1 - 1].x;
   y1 = node[node1 - 1].y;
   x2 = node[node2 - 1].x;
   y2 = node[node2 - 1].y;
   x3 = node[node3 - 1].x;
   y3 = node[node3 - 1].y;
   element[m].cx = (x1 + x2 + x3)/3;
   element[m].cy = (y1 + y2 + y3)/3;
   element[m].s = fabs(x1*y2 - x1*y3 - y1*x2 + y1*x3 + x2*y3 - x3*y2)/2;
   element[m].s1 = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)); 
   element[m].s2 = sqrt((x3 - x2)*(x3 - x2) + (y3 - y2)*(y3 - y2)); 
   element[m].s3 = sqrt((x1 - x3)*(x1 - x3) + (y1 - y3)*(y1 - y3));   
   register double a, b;  
   a = element[m].cx;
   b = element[m].cy;
   register double m1, m2, m3, m4;
   m1 = x2 - x1;
   m2 = y2 - y1;
   m3 = x2 - a;
   m4 = y2 - b;
   register double c, d; 
   c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
   d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
   element[m].ns1x = c/(sqrt(c*c + d*d));
   element[m].ns1y = d/(sqrt(c*c + d*d));
   m1 = x3 - x2;
   m2 = y3 - y2;
   m3 = x3 - a;
   m4 = y3 - b;
   c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
   d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
   element[m].ns2x = c/(sqrt(c*c + d*d));
   element[m].ns2y = d/(sqrt(c*c + d*d));
   m1 = x1 - x3;
   m2 = y1 - y3;
   m3 = x1 - a;
   m4 = y1 - b;
   c = (m1*m1 + m2*m2)*m3 - m3*m1*m1 - m1*m2*m4;
   d = (m1*m1 + m2*m2)*m4 - m4*m2*m2 - m1*m2*m3;
   element[m].ns3x = c/(sqrt(c*c + d*d));
   element[m].ns3y = d/(sqrt(c*c + d*d)); 
   element[m].nes1 = 0;
   element[m].nes2 = 0;
   element[m].nes3 = 0;
 
 // Do the following to minimise time : 
 // 1. Check m != sort2 first and only then move on.
 // 2. Use the below algorithm because it is easily parallelizable:
 /*  if ((te[sort2][1]==node1 || te[sort2][2]==node1 || te[sort2][3]==node1))
     {
      if ((te[sort2][1]==node2 || te[sort2][2]==node2 || te[sort2][3]==node2))
       {
        element[m].nes1 = ....;
        sort_check++;
        if (sort_check....) break; 
       }
      else
       {
        ...
       }
     }
     else if ((te[sort2][1]==node2 || te[sort2][2]==node2 || te[sort2][3]==node2))
     {
      similar to above;
     }
      else if ((te[sort2][1]==node3 || te[sort2][2]==node3 || te[sort2][3]==node3))
     {
      similar to above;
     }
 */
 // 3. Use te or equivalent in order to avoid race condition.
  
   if (element[m].type != 1) // Comment for future : this sorting takes a lot of time. Try to minimise the time.
   {
    int sort_check = 0;
    register int sort2; 
    for (sort2 = 0; sort2 < Number_of_Elements; sort2++)
    {
     if ((te[sort2][1]==node1 || te[sort2][2]==node1 || te[sort2][3]==node1) && (te[sort2][1]==node2 || te[sort2][2]==node2 || te[sort2][3]==node2) && (element[m].number != te[sort2][0]))
     {
      element[m].nes1 = te[sort2][0];
      sort_check++;
      if (sort_check == 3) break;  
     }
     else if ((te[sort2][1]==node2 || te[sort2][2]==node2 || te[sort2][3]==node2) && (te[sort2][1]==node3 || te[sort2][2]==node3 || te[sort2][3]==node3) && (element[m].number != te[sort2][0]))
     {
      element[m].nes2 = te[sort2][0];
      sort_check++;
      if (sort_check == 3) break;  
     }
     else if ((te[sort2][1]==node3 || te[sort2][2]==node3 || te[sort2][3]==node3) && (te[sort2][1]==node1 || te[sort2][2]==node1 || te[sort2][3]==node1) && (element[m].number != te[sort2][0]))
     {
      element[m].nes3 = te[sort2][0]; 
      sort_check++;
      if (sort_check == 3) break; 
     }     
    }  
   }
   else
   {
    register int sort2; // Comment for future : this sorting takes a lot of time. Try to minimise the time.
    for (sort2 = 0; sort2 < Number_of_Elements; sort2++)
    {
     if ((te[sort2][1]==node1 || te[sort2][2]==node1 || te[sort2][3]==node1) && (te[sort2][1]==node2 || te[sort2][2]==node2 || te[sort2][3]==node2) && (element[m].number != te[sort2][0]))
     {
      element[m].nes1 = te[sort2][0];
      break;  
     }     
    }  
   }
   fprintf(fpcw,"%20.18f %20.18f %20.18f %20.18f %20.18f %20.18f %20.18f %20.18f %20.18f %20.18f %20.18f %20.18f %i %i %i\n", element[m].cx, element[m].cy, element[m].s, element[m].s1, element[m].s2, element[m].s3, element[m].ns1x, element[m].ns1y, element[m].ns2x, element[m].ns2y, element[m].ns3x, element[m].ns3y, element[m].nes1, element[m].nes2, element[m].nes3);
  }
  fclose(fpcw);
 }
 else
 {
  FILE *fpcr = fopen("calc.txt","r"); 
  if(!fpcr) 
  {
   perror("Could not find the calc.txt file.");
   exit(0); 
  } 
  int i;
  for (i = 0; i < Number_of_Elements; i++)
  {
   fscanf(fpcr,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %i %i %i", &element[i].cx, &element[i].cy, &element[i].s, &element[i].s1, &element[i].s2, &element[i].s3, &element[i].ns1x, &element[i].ns1y, &element[i].ns2x, &element[i].ns2y, &element[i].ns3x, &element[i].ns3y, &element[i].nes1, &element[i].nes2, &element[i].nes3);  
  } 
  fclose(fpcr);
 }

//----------------------------------------------------------------------------------------------------------------------------------------------

 (*MeshData).update_element = malloc(sizeof(Elements_Updates) * Number_of_Elements);
 Elements_Updates *update_element = (*MeshData).update_element;

 if (((*InputData).readfile == 0) && ((*InputData).readdata == 0)) // Comment for future: do this jsut after calculation of geometry and neighbours. use te[][] for paralleliaztion and elimination of race condition.
 {
  FILE *fpuw;
  fpuw = fopen("ne.txt", "w");
  if (fpuw == NULL)
  {
   printf("File ne.txt could not be generated.\n");
  }
  register int i; 
  for (i = 0; i < Number_of_Elements; i++) 
  { 
   if (element[i].physical_domain == 4)
   {
    register int node1 = element[i].node1;
    register int node2 = element[i].node2;
    register int node3 = element[i].node3;

    int ne1 = element[i].nes1 - 1;
    int ne2 = element[i].nes2 - 1;
    int ne3 = element[i].nes3 - 1;
      
    register int ne1node1 = element[ne1].node1;
    register int ne1node2 = element[ne1].node2;
    register int ne1node3 = element[ne1].node3;         
    register int ne2node1 = element[ne2].node1;
    register int ne2node2 = element[ne2].node2;
    register int ne2node3 = element[ne2].node3;
    register int ne3node1 = element[ne3].node1;
    register int ne3node2 = element[ne3].node2;
    register int ne3node3 = element[ne3].node3;   

    // Finding the side number for the neighbouring element which is sharing the side 1 of the given element

    if(((node1 == ne1node1) && (node2 == ne1node2)) || ((node1 == ne1node2) && (node2 == ne1node1))) 
    {
     update_element[i].nes1update = 0;
    }
    else if(((node1 == ne1node2) && (node2 == ne1node3)) || ((node1 == ne1node3) && (node2 == ne1node2)))
    {
     update_element[i].nes1update = 1;
    }
    else if(((node1 == ne1node3) && (node2 == ne1node1)) || ((node1 == ne1node1) && (node2 == ne1node3)))
    {
     update_element[i].nes1update = 2;
    }

    // Finding the side number for the neighbouring element which is sharing the side 2 of the given element

    if(((node2 == ne2node1) && (node3 == ne2node2)) || ((node2 == ne2node2) && (node3 == ne2node1)))
    {
     update_element[i].nes2update = 0;
    }
    else if(((node2 == ne2node2) && (node3 == ne2node3)) || ((node2 == ne2node3) && (node3 == ne2node2)))
    {
     update_element[i].nes2update = 1;
    }
    else if(((node2 == ne2node3) && (node3 == ne2node1)) || ((node2 == ne2node1) && (node3 == ne2node3)))
    {
     update_element[i].nes2update = 2;
    }

    // Finding the side number for the neighbouring element which is sharing the side 3 of the given element 
 
    if(((node3 == ne3node1) && (node1 == ne3node2)) || ((node3 == ne3node2) && (node1 == ne3node1)))
    {
     update_element[i].nes3update = 0;
    }
    else if(((node3 == ne3node2) && (node1 == ne3node3)) || ((node3 == ne3node3) && (node1 == ne3node2)))
    {
     update_element[i].nes3update = 1;
    }
    else if(((node3 == ne3node3) && (node1 == ne3node1)) || ((node3 == ne3node1) && (node1 == ne3node3)))
    {
     update_element[i].nes3update = 2;
    }
   }
   else
   {
    update_element[i].nes1update = 0; 
    update_element[i].nes2update = 0;
    update_element[i].nes3update = 0;
   }               
   fprintf(fpuw,"%i %i %i\n", update_element[i].nes1update, update_element[i].nes2update, update_element[i].nes3update);
  }
  fclose(fpuw);
 }
 else
 {
  FILE *fpur = fopen("ne.txt","r"); 
  if(!fpur) 
  {
   perror("Could not find the ne.txt file.");
   exit(0); 
  } 
  register int i;
  for (i = 0; i < Number_of_Elements; i++)
  {
   fscanf(fpur,"%i %i %i", &update_element[i].nes1update, &update_element[i].nes2update, &update_element[i].nes3update);  
  } 
  fclose(fpur);
 }

//----------------------------------------------------------------------------------------------------------------------------------------------

 register int minlength;
 for(minlength = 0; minlength < Number_of_Elements; minlength++) 
 {
  register double edl;  
  register double ecx = element[minlength].cx;
  register double ecy = element[minlength].cy;
  register int enes1 = element[minlength].nes1;
  register int enes2 = element[minlength].nes2;
  register int enes3 = element[minlength].nes3;  
  
  // Checking for the smallest side
 
  if (element[minlength].s1 <= element[minlength].s2) 
  {
   edl = element[minlength].s1; 
  }
  else
  {
   edl = element[minlength].s2;
  }
  if (edl >= element[minlength].s3) 
  {
   edl = element[minlength].s3; 
  } 
  
  // Comment for future: check for the largest side also and store it in element structure for using in adaptivity. 
 
  //Checking for the shortest baricenter distance 
 
  register double ds1;
  if (enes1 != 0)
  {
   ds1 = sqrt(((ecx - element[enes1 - 1].cx)*(ecx - element[enes1 - 1].cx))+((ecy - element[enes1 - 1].cy)*(ecy - element[enes1 - 1].cy)));
   
   if (ds1 <= edl)
   {
    edl = ds1;
   }
  }
 
  register double ds2;
  if (enes2 != 0)
  {
   ds2 = sqrt(((ecx - element[enes2 - 1].cx)*(ecx - element[enes2 - 1].cx))+((ecy - element[enes2 - 1].cy)*(ecy - element[enes2 - 1].cy)));
   
   if (ds2 <= edl)
   {
    edl = ds2;
   }
  }
  
  register double ds3;
  if (enes3 != 0)
  {
   ds3 = sqrt(((ecx - element[enes3 - 1].cx)*(ecx - element[enes3 - 1].cx))+((ecy - element[enes3 - 1].cy)*(ecy - element[enes3 - 1].cy)));
   
   if (ds3 <= edl)
   {
    edl = ds3;
   }
  }
  element[minlength].dl = edl;   
 }

//----------------------------------------------------------------------------------------------------------------------------------------------
  
 free(te);
}


