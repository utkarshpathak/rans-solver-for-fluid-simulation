#include "solver.h"
int main()
{ 

time_t begin, end;
begin = time(NULL);
 
Input_Data InputData;
Mesh_Data MeshData;
ReadInputData(&InputData);
ReadMeshData(&MeshData);
CalculateMeshGeometryParameters(&InputData, &MeshData);

end = time(NULL);
printf("Mesh read and geometry parameters calculated (%ld seconds).\n", (long) (end - begin));

CalculateContributingCellCentersToNodeData(&InputData, &MeshData);

end = time(NULL);
printf("Cell-center values/weights contributing to nodes calculated and written (%ld seconds).\n", (long) (end - begin));

CalculateGreenGaussParameters(&InputData, &MeshData);

end = time(NULL);
printf("Parameters for Green-Gauss method calculated (%ld seconds).\n", (long) (end - begin)); 

CalculateReconstructionParameters(&InputData, &MeshData);

end = time(NULL);
printf("Parameters for reconstruction calculated (%ld seconds).\n\n", (long) (end - begin));

CalculateMemoryRequirement(&InputData, &MeshData);

Nodes *node = MeshData.node;
Elements *element = MeshData.element;
Elements_Updates *update_element = MeshData.update_element;
Node_data *nd = MeshData.nd;
Gradient_Parameters *para = MeshData.para; 
Reconstruction_Variables *recon = MeshData.recon;
int Number_of_original_Nodes = MeshData.Number_of_original_Nodes; 
int Number_of_Elements = MeshData.Number_of_Elements; 
int Number_of_Nodes = MeshData.Number_of_Nodes; 
  
//----------------------------------------------------------------------------------------------------------------------------------------------

double ot = 1/3; 
double mu0 = 1.458 * 0.000001;
double cv = 287.1/(InputData.gm - 1);
double cp = cv * InputData.gm;
double Pr = 0.72;

//----------------------------------------------------------------------------------------------------------------------------------------------

// Initialising

double rE_inf = InputData.r_inf * (cv * (InputData.t_inf) + 0.5 * ((InputData.u_inf * InputData.u_inf) + (InputData.v_inf * InputData.v_inf)));
// double rE_inf = (1.0/0.4) + r_inf * 0.5 * ((u_inf * u_inf) + (v_inf * v_inf)); // For the sod shock tube problem

Conserved_Variables *U = malloc(sizeof(Conserved_Variables) * Number_of_Elements);
if (U == NULL)
{
 printf("Could not declare conserved variables. Out of memory.\n");
 exit(1);
}

//double totaltime = 0;
int count;
if (InputData.readfile == 0)
{
 register int initialise;
 for (initialise = 0; initialise < Number_of_Elements; initialise++)
  {
   U[initialise].r = InputData.r_inf;
   U[initialise].ru = InputData.r_inf * InputData.u_inf;
   U[initialise].rv = InputData.r_inf * InputData.v_inf;
   U[initialise].rE = rE_inf; 
  }
 count = 1;   
}
else
{
 FILE *fpread = fopen("flow.txt","r"); 
 if(!fpread) 
 {
  perror("Could not find the flow.txt file.");
  exit(0); 
 } 
 fscanf(fpread,"%i", &count);
 int i;
 for (i = 0; i < Number_of_Elements; i++)
  {
   fscanf(fpread,"%lf %lf %lf %lf", &U[i].r, &U[i].ru, &U[i].rv, &U[i].rE);  
  } 
 fclose(fpread);
 end = time(NULL);
 printf("External file read for initialising of the flow-field (%ld seconds).\n\n", (long) (end - begin));
}

//----------------------------------------------------------------------------------------------------------------------------------------------
/*
// For the Sod shock tube problem

int reinitialise;
for (reinitialise = 0; reinitialise < Number_of_Elements; reinitialise++)
{
 if (element[reinitialise].cx > 0.3)
  {
   U[reinitialise].r = 0.125;
   U[reinitialise].ru = 0.0;
   U[reinitialise].rv = 0.0;
   U[reinitialise].rE = 0.1/0.4;
  }  
} 
*/

//----------------------------------------------------------------------------------------------------------------------------------------------

Interface_Flux *F = malloc(sizeof(Interface_Flux) * Number_of_Elements);
if (F == NULL)
{
 printf("Could not declare interface fluxes. Out of memory.\n");
 exit(1);
}

//----------------------------------------------------------------------------------------------------------------------------------------------

Node_Values *nv = malloc(sizeof(Node_Values) * Number_of_original_Nodes);
if (nv == NULL)
{
 printf("Could not declare node value variables. Out of memory.\n");
 exit(1);
}

//----------------------------------------------------------------------------------------------------------------------------------------------

FILE *fpresid;
fpresid = fopen("flow.dat", "w");
if (fpresid == NULL)
{
 printf("File flow.dat could not be generated.\n");
}

int* pointer_for_side_update = NULL; // Comment for future important : initialize any newly declared pointer to NULL if not initialised already.
   
//double residual = 100000000.0;
while (count < (InputData.iterations + 1))
//while (totaltime < 0.2)
{ 

 // Re-initialise the interface flux matrix of conserved variables
 
 //memset(F,'\0',sizeof(Interface_Flux) * Number_of_Elements);
 
 
 {
 register int F_update;                                                
 for (F_update = 0; F_update < Number_of_Elements; F_update++) 
 {
  F[F_update].fr = 0.0; 
  F[F_update].fru = 0.0;
  F[F_update].frv = 0.0;
  F[F_update].frE = 0.0; 
 }
 }
  
 // Re-initialise the update values of all 3 sides
 
 {
 register int S_update;  
 for (S_update = 0; S_update < Number_of_Elements; S_update++)  
 {
  update_element[S_update].updates1 = 0; 
  update_element[S_update].updates2 = 0;
  update_element[S_update].updates3 = 0; 
 }
 }   

//----------------------------------------------------------------------------------------------------------------------------------------------

// CalculateFlowVariablesAtNodes()...

{ 
 register int nodecount; 
 for (nodecount = 0; nodecount < Number_of_original_Nodes; nodecount++)
 {
  register double num_sum_r, num_sum_ru, num_sum_rv, num_sum_rE, den_sum;
  num_sum_r = 0;
  num_sum_ru = 0;
  num_sum_rv = 0;
  num_sum_rE = 0;
  den_sum = 0; 
  register double weight;   
  register int f, u, i;
  i = nd[nodecount].n;
  for (f = 0; f < i; f++)
  {
   u = nd[nodecount].cc[f].num;
   weight = nd[nodecount].cc[f].wv;
   num_sum_r = num_sum_r + weight * U[u].r; 
   num_sum_ru = num_sum_ru + weight * U[u].ru; 
   num_sum_rv = num_sum_rv + weight * U[u].rv; 
   num_sum_rE = num_sum_rE + weight * U[u].rE; 
   den_sum = den_sum + weight;       
  } 
  nv[nodecount].r = num_sum_r / den_sum;
  nv[nodecount].ru = num_sum_ru / den_sum;
  nv[nodecount].rv = num_sum_rv / den_sum;
  nv[nodecount].rE = num_sum_rE / den_sum;    
 }
}
 
//----------------------------------------------------------------------------------------------------------------------------------------------
 
// Calculating fluxes for all elements 
 
 register int i;  
 
 //for (i = Number_of_Elements - 1; i >= 0; --i)
 for (i = 0; i < Number_of_Elements; i++)
  {
   
   register int ne1 = element[i].nes1 - 1;
   register int ne2 = element[i].nes2 - 1;
   register int ne3 = element[i].nes3 - 1;
       
   if (element[i].physical_domain == 4) // Element inside the domain (not on the boundary)
    {             
        register double FHLLC[4];          
        register double vF[4];  
        
        register double nx;
        register double ny;
        register double length;
        register int ne;

        register double rl;
        register double ul;
        register double vl; 
        register double el;
        register double pl;
        register double hl;
        register double ql;

        register double rr;
        register double ur;
        register double vr; 
        register double er;
        register double pr;        
        register double hr;
        register double qr;
        
        int side;
        for(side = 1; side <= 3; side++)
        {   
            if (side == 1 && update_element[i].updates1 == 0)
            {
                nx = element[i].ns1x;
                ny = element[i].ns1y;
                length = element[i].s1;
                ne = ne1;                             
            }
            else if (side == 2 && update_element[i].updates2 == 0)
            {
                nx = element[i].ns2x;
                ny = element[i].ns2y;
                length = element[i].s2;
                ne = ne2;
            }
            else if (side == 3 && update_element[i].updates3 == 0)
            {
                nx = element[i].ns3x;
                ny = element[i].ns3y;
                length = element[i].s3;
                ne = ne3;
            } 
            else continue;

            rl = U[i].r;
            ul = U[i].ru / rl;   
            vl = U[i].rv / rl;
            el = U[i].rE / rl;
            pl = (InputData.gm - 1) * (U[i].rE - 0.5 * rl * (ul * ul + vl * vl));
            hl = el + pl / rl;
            ql = ul * nx + vl * ny;
            
            rr = U[ne].r;
            ur = U[ne].ru / rr;   
            vr = U[ne].rv / rr;
            er = U[ne].rE / rr;
            pr = (InputData.gm - 1) * (U[ne].rE - 0.5 * rr * (ur * ur + vr * vr));
            hr = er + pr / rr;
            qr = ur * nx + vr * ny;
                                                
            // Roe estimate for wave speeds
                  
            double roel = sqrt(rl);
            double roer = sqrt(rr);
            double roesum = roel + roer;
            roel = roel / roesum; 
            roer = roer / roesum;
            
            double utilde = roel * ql + roer * qr;
            double htilde = roel * hl + roer * hr;
            double atilde = sqrt((InputData.gm - 1) * (htilde - 0.5 * utilde * utilde));
             
            register double sl = utilde - atilde;
            register double sr = utilde + atilde;
            register double sm = (rr*qr*(sr-qr)-rl*ql*(sl-ql)+pl-pr)/(rr*(sr-qr)-rl*(sl-ql));
            register double pstar = rl * (ql - sl) * (ql - sm) + pl;
            
            // Calculating HLLC flux
            
            if (sl >= 0)
            {
                register double tmp = rl * ql;
                FHLLC[0] = tmp;
                FHLLC[1] = tmp * ul + pl * nx;
                FHLLC[2] = tmp * vl + pl * ny;
                FHLLC[3] = tmp * hl; 
            }
            else if (sr <= 0)
            {
                register double tmp = rr * qr;
                FHLLC[0] = tmp;
                FHLLC[1] = tmp * ur + pr * nx;
                FHLLC[2] = tmp * vr + pr * ny;
                FHLLC[3] = tmp * hr;
            }
            else if (sl <= 0 && sm >= 0)
            {
                register double tmp = 1/(sl-sm);
                register double tmp1 = rl*(sl-ql)*tmp;
                register double tmp2 = (pstar-pl)*tmp;

                register double Ulstar[4];
                Ulstar[0] = tmp1;
                Ulstar[1] = tmp1 * ul + tmp2 * nx;
                Ulstar[2] = tmp1 * vl + tmp2 * ny;
                Ulstar[3] = tmp1 * el - pl * ql * tmp + pstar * sm * tmp;
                
                FHLLC[0] = Ulstar[0] * sm;
                FHLLC[1] = Ulstar[1] * sm + pstar * nx;
                FHLLC[2] = Ulstar[2] * sm + pstar * ny;
                FHLLC[3] = (Ulstar[3] + pstar) * sm; 
            }
            else if (sm <= 0 && sr >= 0)
            {
                register double tmp = 1/(sr-sm);
                register double tmp1 = rr*(sr-qr)*tmp;
                register double tmp2 = (pstar-pr)*tmp;

                register double Urstar[4];
                Urstar[0] = tmp1;
                Urstar[1] = tmp1 * ur + tmp2 * nx;
                Urstar[2] = tmp1 * vr + tmp2 * ny;
                Urstar[3] = tmp1 * er - pr * qr * tmp + pstar * sm * tmp;
                
                FHLLC[0] = Urstar[0] * sm;
                FHLLC[1] = Urstar[1] * sm + pstar * nx;
                FHLLC[2] = Urstar[2] * sm + pstar * ny;
                FHLLC[3] = (Urstar[3] + pstar) * sm; 
            }
            else
            {
                printf("Error in calculating HLLC flux. count: %i, element: %i, side: %i\n", count, i, side);     
            }
            
            // Updating flux 
         
            FHLLC[0] = length * FHLLC[0];
            FHLLC[1] = length * FHLLC[1];
            FHLLC[2] = length * FHLLC[2];
            FHLLC[3] = length * FHLLC[3]; 
         
            F[i].fr = F[i].fr + FHLLC[0]; 
            F[i].fru = F[i].fru + FHLLC[1];
            F[i].frv = F[i].frv + FHLLC[2];
            F[i].frE = F[i].frE + FHLLC[3];
                        
            if (InputData.viscous != 0)
            {           
                double uS = 0.5 * (ul + ur);
                double vS = 0.5 * (vl + vr); 
                register double tS = 0.5 * ((el - 0.5 * (ul*ul+vl*vl)) + (er - 0.5 * (ur*ur + vr*vr))) / cv;
                          
                register double muS = (sqrt(tS * tS * tS) * mu0) / (tS + 110.4); 
                double tc = muS * cp / Pr;

                double gradients[6]; // 0 = dudx, 1 = dudy, 2 = dvdx, 3 = dvdy, 4 = dtdx, 5 = dtdy         
                GradientAtCellBoundary(side, i, gradients, U, para, nv, element);
                          
                register double dudx = gradients[0]; 
                register double dvdy = gradients[3];
                                    
                double txx = 2 * muS * (dudx - ot * (dudx + dvdy)); 
                double tyy = 2 * muS * (dvdy - ot * (dudx + dvdy));                     
                register double txy = muS * (gradients[1] + gradients[2]);     

                vF[1] = txx * nx + txy * ny;
                vF[2] = txy * nx + tyy * ny;
                vF[3] = (uS*txx+vS*txy+tc*gradients[4]) * nx + (uS*txy+vS*tyy+tc*gradients[5]) * ny;     
                
                vF[1] = length * vF[1];
                vF[2] = length * vF[2];
                vF[3] = length * vF[3];                                                                 
                                                 
                F[i].fru = F[i].fru - vF[1];
                F[i].frv = F[i].frv - vF[2];
                F[i].frE = F[i].frE - vF[3];                                                                  
            }
                        
            // Updating the flux of neighbouring element
         
            if (element[ne].physical_domain == 4)
            {
                F[ne].fr = F[ne].fr - FHLLC[0];
                F[ne].fru = F[ne].fru - FHLLC[1];
                F[ne].frv = F[ne].frv - FHLLC[2];
                F[ne].frE = F[ne].frE - FHLLC[3];

                if (InputData.viscous != 0)
                {                       
                    F[ne].fru = F[ne].fru + vF[1];
                    F[ne].frv = F[ne].frv + vF[2];
                    F[ne].frE = F[ne].frE + vF[3];                                 
                }                 
            }
 
            if (side == 1)
            {
                update_element[i].updates1 = 1;
                if (element[ne].physical_domain == 4)
                {
                    pointer_for_side_update = &update_element[ne1].updates1;
                    *(pointer_for_side_update + update_element[i].nes1update) = 1;
                }
            }
            else if (side == 2)
            {
                update_element[i].updates2 = 1;
                if (element[ne].physical_domain == 4)
                {
                    pointer_for_side_update = &update_element[ne2].updates1;
                    *(pointer_for_side_update + update_element[i].nes2update) = 1;
                }
            }
            else if (side == 3)
            {
                update_element[i].updates3 = 1;
                if (element[ne].physical_domain == 4)
                {
                    pointer_for_side_update = &update_element[ne3].updates1;
                    *(pointer_for_side_update + update_element[i].nes3update) = 1;
                }
            } 
        }                         
    } // Calculating in the fluid domain ends here.
    
   else if (element[i].physical_domain == 2) // Extrapolation boundary condition
    {
     U[i].r = U[ne1].r; 
     U[i].ru = U[ne1].ru; 
     U[i].rv = U[ne1].rv; 
     U[i].rE = U[ne1].rE;           
    }
   
   else if (element[i].physical_domain == 3) 
    {
     
     if (InputData.viscous == 0)
     {
      // Inviscid wall boundary condition
    
      double u2 = U[ne1].ru/U[ne1].r;
      double v2 = U[ne1].rv/U[ne1].r;
    
      U[i].r = U[ne1].r;
      U[i].ru = U[i].r * (u2 - 2 * u2 * element[i].ns1x * element[i].ns1x - 2 * v2 * element[i].ns1x * element[i].ns1y);
      U[i].rv = U[i].r * (v2 - 2 * u2 * element[i].ns1x * element[i].ns1y - 2 * v2 * element[i].ns1y * element[i].ns1y);
      U[i].rE = U[ne1].rE;
     }
            
     else if (InputData.viscous != 0)
     {     
      // Viscous wall boundary condition
          
      double u2 = U[ne1].ru/U[ne1].r;
      double v2 = U[ne1].rv/U[ne1].r;
      double t2 = ((U[ne1].rE/U[ne1].r)-0.5*(u2*u2+v2*v2))/cv;
      double t1 = 2 * InputData.twal - t2;
      U[i].r = U[ne1].r * t2 / t1;      
      U[i].ru = - U[i].r * u2;
      U[i].rv = - U[i].r * v2;
      U[i].rE = U[i].r * (cv * t1 + 0.5 * (u2 * u2 + v2 * v2)); // u1^2 = u2^2 and v1^2 = v2^2                                            
     }
     
    }
   
   else if (element[i].physical_domain == 1) // Element at the inlet boundary
    {     
     /*
     // for the sod shock tube problem

     U[i].r = U[ne1].r;
     U[i].ru = U[ne1].ru;
     U[i].rv = U[ne1].rv;
     U[i].rE = U[ne1].rE;
     */
          
     // For freestream inlet boundary condition
     
     U[i].r = InputData.r_inf;
     U[i].ru = InputData.r_inf * InputData.u_inf;
     U[i].rv = InputData.r_inf * InputData.v_inf;
     U[i].rE = rE_inf; 
    }
 
  } // for loop ends here (all elements checked one by one).

//----------------------------------------------------------------------------------------------------------------------------------------------

// Find the time step for the domain (one time step size for all elements)

 register double dt = 1000000.0; // Comment for future : look into the idea of different time-step sizes for all elements.
 {
 register double dt_temp;
 register int timestep; 
 for (timestep = 0; timestep < Number_of_Elements; timestep++) 
 {
  register double r = U[timestep].r;
  register double u = U[timestep].ru/r;
  register double v = U[timestep].rv/r;
  register double vel_square = u * u + v * v;  
  dt_temp = InputData.CFL * element[timestep].dl / (sqrt(vel_square) + sqrt(InputData.gm * (InputData.gm - 1) * (U[timestep].rE/r - 0.5 * vel_square)));
  if (dt_temp <= dt) dt = dt_temp;
   
 }
 }

//----------------------------------------------------------------------------------------------------------------------------------------------

 {  
 register int U_update;                                    
 for (U_update = 0; U_update < Number_of_Elements; U_update++)  
 {
 
  // Update the conserved variables for the next iteration

  U[U_update].r = U[U_update].r - (dt / element[U_update].s) * F[U_update].fr;
  U[U_update].ru = U[U_update].ru - (dt / element[U_update].s) * F[U_update].fru;
  U[U_update].rv = U[U_update].rv - (dt / element[U_update].s) * F[U_update].frv;
  U[U_update].rE = U[U_update].rE - (dt / element[U_update].s) * F[U_update].frE; 
    
 }
 }

//----------------------------------------------------------------------------------------------------------------------------------------------

// Residual calculation 

// Comment for future : can put an option to choose whether calculation of residual should be done or not. Would inrease speed.
 
 {
 register double absflux;
 register double res;
 register double rms = 0.0; 
 register double resmax = 0.0;
 register int ele_max_res = 1;
 register double rmsres;  
 {
 register int res_calc;                                    
 for (res_calc = 0; res_calc < Number_of_Elements; res_calc++)  
 {
  absflux = F[res_calc].fr;
  if (absflux < 0.0)
   {
    absflux = absflux * -1.0;
   }
  res = absflux/element[res_calc].s;
  if (resmax <= res)
   {
    resmax = res;
    ele_max_res = element[res_calc].number;
   }
  rms = rms + res * res; 
 }
 } 
 rmsres = sqrt(rms) / (InputData.r_inf * Number_of_Elements); 
 printf("%i\t%lf\t%lf/%lf\n", count, rmsres, element[ele_max_res - 1].cx, element[ele_max_res - 1].cy);

//----------------------------------------------------------------------------------------------------------------------------------------------

// Writing of residual and temporary flow files 

 fprintf(fpresid,"%i\t%lf\n",count, rmsres); // Comment for future : option of writing the files. Would increase speed.
 
 if ((InputData.writetemporaryfile != 0) && (count % InputData.writingfrequency == 0))
  {
   printf("\nWriting temporary files.\n\n"); 
   FILE *fpwrite;
   fpwrite = fopen("flow.txt", "w");
   if (fpwrite == NULL)
    {
     printf("File flow.txt could not be generated.\n");
    }
   fprintf(fpwrite,"%i\n", count + 1);
   register int tmp;
   for (tmp = 0; tmp < Number_of_Elements; tmp++)
    { 
     fprintf(fpwrite,"%lf %lf %lf %lf\n", U[tmp].r, U[tmp].ru, U[tmp].rv, U[tmp].rE);
    }
   fclose(fpwrite);    
  }
 }
 
//----------------------------------------------------------------------------------------------------------------------------------------------
 
 //totaltime = totaltime + dt; 
 count++;

} // while loop ends here.

free(nv);
fclose(fpresid);
free(recon);
free(para);
free(F);

end = time(NULL);
printf("\nFlux calculation completed (%ld seconds).\n", (long) (end - begin));

//----------------------------------------------------------------------------------------------------------------------------------------------

// Converting the solution to primitive variables and simultaneously writing the flow.txt file (for running future simulations)

Primitive_Variables *up = malloc(sizeof(Primitive_Variables) * Number_of_Elements);
if (up == NULL)
{
 printf("Could not declare primitive variables. Out of memory.\n");
 exit(1);
}

FILE *fpwritefinal;
fpwritefinal = fopen("flow.txt", "w");
if (fpwritefinal == NULL)
{
 printf("File flow.txt could not be generated.\n");
}
fprintf(fpwritefinal,"%i\n", count);

int prim;
for (prim = 0; prim < Number_of_Elements; prim++) // Comment for future : prim can be declared with register storage class as its used regularly. Put whole thing in {}.
{ 
 up[prim].r = U[prim].r;
 up[prim].u = U[prim].ru / U[prim].r;
 up[prim].v = U[prim].rv / U[prim].r;
 up[prim].t = ((U[prim].rE / U[prim].r) - 0.5 * ((up[prim].u * up[prim].u) + (up[prim].v * up[prim].v))) / cv;

 fprintf(fpwritefinal,"%lf %lf %lf %lf\n", U[prim].r, U[prim].ru, U[prim].rv, U[prim].rE);
}

printf("File flow.txt generated.\n");
fclose(fpwritefinal);
free(U);

//----------------------------------------------------------------------------------------------------------------------------------------------

// calculating the parameters for flow visualization

Unstructured_File_Format *vtu = malloc(sizeof(Unstructured_File_Format) * Number_of_Elements);
if (vtu == NULL)
{
 printf("Could not declare unstructured file format variables. Out of memory.\n");
 exit(1);
}

int element_data; // Comment for future : element_data can be declared with register storage class as its used regularly. Put whole thing in {}.
for (element_data = 0; element_data < Number_of_Elements; element_data++) 
{
 vtu[element_data].number = element[element_data].number;
 vtu[element_data].type = element[element_data].type;
 vtu[element_data].node1 = element[element_data].node1;
 vtu[element_data].node2 = element[element_data].node2;
 vtu[element_data].node3 = element[element_data].node3;
 vtu[element_data].cx = element[element_data].cx;
 vtu[element_data].cy = element[element_data].cy;
}

int primitive; // Comment for future : primitive can be declared with register storage class as its used regularly. Put whole thing in {}.
for (primitive = 0; primitive < Number_of_Elements; primitive++)
{
 if (element[primitive].type != 1)
  {
   vtu[primitive].ccr = up[primitive].r;  
   vtu[primitive].ccu = up[primitive].u;
   vtu[primitive].ccv = up[primitive].v;
   vtu[primitive].cct = up[primitive].t;
  }
 else
  {
   vtu[primitive].ccr = 0.5 * (up[primitive].r + up[element[primitive].nes1 - 1].r);  
   vtu[primitive].ccu = 0.5 * (up[primitive].u + up[element[primitive].nes1 - 1].u);
   vtu[primitive].ccv = 0.5 * (up[primitive].v + up[element[primitive].nes1 - 1].v);
   vtu[primitive].cct = 0.5 * (up[primitive].t + up[element[primitive].nes1 - 1].t);
  }
}

free(update_element);
free(element);

Point_Values *pt = malloc(sizeof(Point_Values) * Number_of_original_Nodes);
if (pt == NULL)
{
 printf("Could not declare point value variables. Out of memory.\n");
 exit(1);
}

int points; // Comment for future : points can be declared with register storage class as its used regularly. Put whole thing in {}.
for (points = 0; points < Number_of_original_Nodes; points++)
{
 double num_sum_r, num_sum_u, num_sum_v, num_sum_t, den_sum;
 num_sum_r = 0;
 num_sum_u = 0;
 num_sum_v = 0;
 num_sum_t = 0;
 den_sum = 0; 
 double wn;   
 int f, u, i;
 i = nd[points].n;
 for (f = 0; f < i; f++)
  {
   u = nd[points].cc[f].num;
   wn = nd[points].cc[f].wv;
   num_sum_r = num_sum_r + wn * up[u].r; 
   num_sum_u = num_sum_u + wn * up[u].u; 
   num_sum_v = num_sum_v + wn * up[u].v; 
   num_sum_t = num_sum_t + wn * up[u].t; 
   den_sum = den_sum + wn;       
  } 
 pt[points].pvr = num_sum_r / den_sum;
 pt[points].pvu = num_sum_u / den_sum;
 pt[points].pvv = num_sum_v / den_sum;
 pt[points].pvt = num_sum_t / den_sum;    
}

free(up);

// Freeing the nd[].cc memory

int freemem; // Comment for future : freemem can be declared with register storage class as its used regularly. Put whole thing in {}.
for (freemem = 0; freemem < Number_of_original_Nodes; freemem++)
 {
  free(nd[freemem].cc);   
 } 
free(nd);

end = time(NULL);
printf("Cell and point data calculation completed (%ld seconds).\n", (long) (end - begin));

//----------------------------------------------------------------------------------------------------------------------------------------------

// Writing of the files for flow visualization

FILE *fp;
fp = fopen("flow.vtu", "w");

if (fp == NULL)
{
 printf("File flow.vtu could not be generated.\n");
}
fprintf(fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
fprintf(fp,"  <UnstructuredGrid>\n");
fprintf(fp,"    <Piece NumberOfPoints=\"%i\" NumberOfCells=\"%i\">\n",Number_of_original_Nodes,Number_of_Elements);
fprintf(fp,"      <PointData Scalars=\"scalars\">\n");

//----------------------------------------------------------------------------------------------------------------------------------------------

// Point data writing

fprintf(fp,"        <DataArray type=\"Float32\" Name=\"Density\" Format=\"ascii\">\n");
int pdata_r;
for (pdata_r = 0; pdata_r < Number_of_original_Nodes; pdata_r++)
{
 fprintf(fp,"          %lf\n",pt[pdata_r].pvr);
}
fprintf(fp,"        </DataArray>\n");
//----------------------------------------------------------------------------------------------------------------------------------------------
fprintf(fp,"        <DataArray type=\"Float32\" Name=\"x_velocity\" Format=\"ascii\">\n");
int pdata_u;
for (pdata_u = 0; pdata_u < Number_of_original_Nodes; pdata_u++)
{
 fprintf(fp,"          %lf\n",pt[pdata_u].pvu);
}
fprintf(fp,"        </DataArray>\n");
//----------------------------------------------------------------------------------------------------------------------------------------------
fprintf(fp,"        <DataArray type=\"Float32\" Name=\"y_velocity\" Format=\"ascii\">\n");
int pdata_v;
for (pdata_v = 0; pdata_v < Number_of_original_Nodes; pdata_v++)
{
 fprintf(fp,"          %lf\n",pt[pdata_v].pvv);
}
fprintf(fp,"        </DataArray>\n");
//----------------------------------------------------------------------------------------------------------------------------------------------
fprintf(fp,"        <DataArray type=\"Float32\" Name=\"Temperature\" Format=\"ascii\">\n");
int pdata_t;
for (pdata_t = 0; pdata_t < Number_of_original_Nodes; pdata_t++)
{
 fprintf(fp,"          %lf\n",pt[pdata_t].pvt);
}
fprintf(fp,"        </DataArray>\n");
//----------------------------------------------------------------------------------------------------------------------------------------------
fprintf(fp,"        <DataArray type=\"Float32\" Name=\"velocity\" NumberOfComponents=\"3\" Format=\"ascii\">\n");
int pdata_vel;
for (pdata_vel = 0; pdata_vel < Number_of_original_Nodes; pdata_vel++)
{
 fprintf(fp,"          %lf\t%lf\t%lf\n",pt[pdata_vel].pvu,pt[pdata_vel].pvv,0.0);
}
fprintf(fp,"        </DataArray>\n");
fprintf(fp,"      </PointData>\n");

free(pt);

//----------------------------------------------------------------------------------------------------------------------------------------------

// Cell data writing

fprintf(fp,"      <CellData Scalars=\"scalars\">\n");
fprintf(fp,"        <DataArray type=\"Float32\" Name=\"Density\" Format=\"ascii\">\n");
int cdata_r;
for (cdata_r = 0; cdata_r < Number_of_Elements; cdata_r++)
{
 fprintf(fp,"          %lf\n",vtu[cdata_r].ccr);
}
fprintf(fp,"        </DataArray>\n");
//----------------------------------------------------------------------------------------------------------------------------------------------
fprintf(fp,"        <DataArray type=\"Float32\" Name=\"x_velocity\" Format=\"ascii\">\n");
int cdata_u;
for (cdata_u = 0; cdata_u < Number_of_Elements; cdata_u++)
{
 fprintf(fp,"          %lf\n",vtu[cdata_u].ccu);
}
fprintf(fp,"        </DataArray>\n");
//----------------------------------------------------------------------------------------------------------------------------------------------
fprintf(fp,"        <DataArray type=\"Float32\" Name=\"y_velocity\" Format=\"ascii\">\n");
int cdata_v;
for (cdata_v = 0; cdata_v < Number_of_Elements; cdata_v++)
{
 fprintf(fp,"          %lf\n",vtu[cdata_v].ccv);
}
fprintf(fp,"        </DataArray>\n");
//----------------------------------------------------------------------------------------------------------------------------------------------
fprintf(fp,"        <DataArray type=\"Float32\" Name=\"Temperature\" Format=\"ascii\">\n");
int cdata_t;
for (cdata_t = 0; cdata_t < Number_of_Elements; cdata_t++)
{
 fprintf(fp,"          %lf\n",vtu[cdata_t].cct);
}
fprintf(fp,"        </DataArray>\n");
//----------------------------------------------------------------------------------------------------------------------------------------------
fprintf(fp,"        <DataArray type=\"Float32\" Name=\"velocity\" NumberOfComponents=\"3\" Format=\"ascii\">\n");
int cdata_vel;
for (cdata_vel = 0; cdata_vel < Number_of_Elements; cdata_vel++)
{
 fprintf(fp,"          %lf\t%lf\t%lf\n",vtu[cdata_vel].ccu,vtu[cdata_vel].ccv,0.0);
}
fprintf(fp,"        </DataArray>\n");
fprintf(fp,"      </CellData>\n");

//----------------------------------------------------------------------------------------------------------------------------------------------

// Node data writing

fprintf(fp,"      <Points>\n");
fprintf(fp,"        <DataArray type=\"Float32\" NumberOfComponents=\"3\" Format=\"ascii\">\n");
int ndata;
for (ndata = 0; ndata < Number_of_original_Nodes; ndata++)
{
 fprintf(fp,"          %lf  %lf  %lf\n",node[ndata].x,node[ndata].y,node[ndata].z);
}
fprintf(fp,"        </DataArray>\n");
fprintf(fp,"      </Points>\n");
fprintf(fp,"      <Cells>\n");
fprintf(fp,"        <DataArray type=\"Int32\" Name=\"connectivity\" Format=\"ascii\">\n");

int conndata;
for (conndata = 0; conndata < Number_of_Elements; conndata++)
{
 if (vtu[conndata].type == 1)
  {
   fprintf(fp,"          %i  %i\n",vtu[conndata].node1 - 1,vtu[conndata].node2 - 1);
  }
 else
  {
   fprintf(fp,"          %i  %i  %i\n",vtu[conndata].node1 - 1,vtu[conndata].node2 - 1,vtu[conndata].node3 - 1);
  }
}
fprintf(fp,"        </DataArray>\n");
fprintf(fp,"        <DataArray type=\"Int32\" Name=\"offsets\" Format=\"ascii\">\n");

int offsetsum;
offsetsum = vtu[0].type + 1;
fprintf(fp,"          %i  ",offsetsum);
int offsetdata;
for (offsetdata = 1; offsetdata < Number_of_Elements; offsetdata++)
{
 offsetsum = offsetsum + vtu[offsetdata].type + 1;  
 fprintf(fp,"%i  ",offsetsum);
}
fprintf(fp,"\n");
fprintf(fp,"        </DataArray>\n");
fprintf(fp,"        <DataArray type=\"Int32\" Name=\"types\" Format=\"ascii\">\n");

fprintf(fp,"          %i  ",2 * vtu[0].type + 1);
int typedata;
for (typedata = 1; typedata < Number_of_Elements; typedata++)
{
 fprintf(fp,"%i  ",2 * vtu[typedata].type + 1);
}
fprintf(fp,"\n");
fprintf(fp,"        </DataArray>\n");
fprintf(fp,"      </Cells>\n");
fprintf(fp,"    </Piece>\n");
fprintf(fp,"  </UnstructuredGrid>\n");
fprintf(fp,"</VTKFile>\n");

fclose(fp);

//----------------------------------------------------------------------------------------------------------------------------------------------

free(vtu);
free(node); 

end = time(NULL);
printf("flow.vtu file generated with cell and point data (%ld seconds).\n", (long) (end - begin));

//----------------------------------------------------------------------------------------------------------------------------------------------

return 0;  
}
