# RANS solver for fluid simulation

This program solves RANS equations on unstructured triangular meshes. Mesh generation and visualization are done using Gmsh and Paraview respectively.

Create the executable using make command. Then run the solver using ./solver
