#include "solver.h"

void CalculateMemoryRequirement(Input_Data *InputData, Mesh_Data *MeshData)
{
 // Memory requirement calculation

 Node_data *nd = (*MeshData).nd;
 register int Number_of_original_Nodes = (*MeshData).Number_of_original_Nodes; 
 register double memsize = 0;
 register int cc = sizeof(Cell_center);
 register int nodememory;
 for (nodememory = 0; nodememory < Number_of_original_Nodes; nodememory++)
 {
  memsize = memsize + cc * nd[nodememory].n;        
 }
 memsize = memsize + (sizeof(Nodes) * (*MeshData).Number_of_Nodes + (sizeof(Elements) + (*InputData).viscous * sizeof(Gradient_Parameters) + sizeof(Conserved_Variables) + sizeof(Interface_Flux) + sizeof(Unstructured_File_Format) + (*InputData).viscous * sizeof(Reconstruction_Variables)) * (*MeshData).Number_of_Elements + sizeof(Node_data) * (*MeshData).Number_of_original_Nodes);
 printf("Approximate memory requirement is %lf MB.\n\n", memsize/(1024.0 * 1024.0));
}
